namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using System.Security.Claims;
    using System.Security.Principal;
    using App.Configuration;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Public.Controllers;
    using Server.Registration;

    public abstract class BaseAuthenticationModule : RegistrationBase, IModule
    {
        public IReadOnlyList<IEndpointConfiguration> Endpoints { get; }
            = new List<IEndpointConfiguration>();
        /*{
            new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignIn)),
                Url = "auth/sign-in",
            },
            new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignInByType)),
                Url = "auth/sign-in/{authProvider}",
            },
            new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignOut)),
                Url = "auth/sign-out",
            }
        };*/

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IReadOnlyList<ITaskConfiguration> Tasks { get; } = new List<ITaskConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects { get; } = new List<ITypeInstanceConfiguration>();
        public IHealthChecksConfiguration HealthChecks { get; } = new MyHc();

        public IApplicationConfiguration Application { get; } = new MyApp();

        public IReadOnlyList<IInjectionItemConfiguration> Injection { get; } = new List<IInjectionItemConfiguration>();
        public IReadOnlyList<ICorsItemConfiguration> Cors { get; } = new List<ICorsItemConfiguration>();

        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterFilters(builder, services, config);

            builder.AddMvcOptions(x => x.Filters.Add(new LoginExceptionFilter(this.DefaultAuthenticationScheme, this.CookieName)));
        }

        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            if (router.Routes.Any(x => x.Controller == typeof(PrimaryController)))
            {
                return;
            }

            base.RegisterRoutes(router, provider, config);
            /*new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignIn)),
                Url = "auth/sign-in",
            },
            new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignInByType)),
                Url = "auth/sign-in/{authProvider}",
            },
            new EndpointConfiguration
            {
                Type = typeof(PrimaryController),
                Method = typeof(PrimaryController).GetMethod(nameof(PrimaryController.GetSignOut)),
                Url = "auth/sign-out",
            }*/
            var ctlr = typeof(PrimaryController);
            var props = new Dictionary<string, object>();
            var parms = new Dictionary<string, object>();
            // {
            //     {"modules", provider.GetServices<IAuthenticationScheme>().ToArray()}
            // };
            var get = HttpMethod.Get;
            var ep = RouteType.Endpoint;
            router.MapRoute(get, "auth/sign-in", ctlr, ctlr.GetMethod(nameof(PrimaryController.GetSignIn)), parms, props, ep);
            router.MapRoute(get, "auth/sign-in/{authProvider}", ctlr, ctlr.GetMethod(nameof(PrimaryController.GetSignInByType)), parms, props, ep);
            router.MapRoute(get, "auth/sign-out", ctlr, ctlr.GetMethod(nameof(PrimaryController.GetSignOut)), parms, props, ep);
            router.MapRoute(get, "auth/debug", ctlr, ctlr.GetMethod(nameof(PrimaryController.Debug)), parms, props, ep);
        }

        public virtual string CookieName { get; } = "base2art.auth.1";

        public abstract string DefaultAuthenticationScheme { get; }

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.UseForwardedHeaders();
            app.UseAuthentication();
        }

        protected AuthenticationBuilder GetCookiesAuthBuilder(IMvcCoreBuilder builder)
        {
            return builder.Services.AddAuthentication("Cookies")
                          .AddCookie(o =>
                          {
                              o.Cookie.Name = this.CookieName;
                              o.LoginPath = "/auth/sign-in";
                              o.LogoutPath = "/auth/sign-out";
                              o.ExpireTimeSpan = TimeSpan.FromDays(7);
                              o.Cookie.HttpOnly = true;

#if NETCOREAPP3_1_OR_GREATER
                              o.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
#endif
                          });
        }

        private class EndpointConfiguration : IEndpointConfiguration
        {
            public Type Type { get; set; }
            public IReadOnlyDictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
            public IReadOnlyDictionary<string, object> Properties { get; } = new Dictionary<string, object>();
            public MethodInfo Method { get; set; }
            public HttpVerb Verb { get; set; } = HttpVerb.Get;
            public string Url { get; set; }
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new ITypeInstanceConfiguration[0];
        }

        private class MyHc : IHealthChecksConfiguration
        {
            public IReadOnlyList<IHealthCheckConfiguration> Items { get; } = new List<IHealthCheckConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new List<ITypeInstanceConfiguration>();
        }

        private class MyApp : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters { get; } = new List<ITypeInstanceConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters { get; } = new List<ITypeInstanceConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters { get; } = new List<ITypeInstanceConfiguration>();

            public IReadOnlyList<IModelBindingConfiguration> ModelBindings { get; } = new IModelBindingConfiguration[]
                                                                                      {
                                                                                          new ModelBindingConfiguration(typeof(IPrincipal),
                                                                                              typeof(ClaimsPrincipalBinder
                                                                                              )),
                                                                                          new ModelBindingConfiguration(typeof(ClaimsPrincipal),
                                                                                              typeof(ClaimsPrincipalBinder
                                                                                              ))
                                                                                      };

            public IReadOnlyDictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions { get; } = new MyExConfig();

            private class MyExConfig : IExceptionConfiguration
            {
                public bool RegisterCommonHandlers { get; } = false;
                public bool AllowStackTraceInOutput { get; } = false;
                public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; } = new List<ITypeInstanceConfiguration>();
            }
        }
    }
}