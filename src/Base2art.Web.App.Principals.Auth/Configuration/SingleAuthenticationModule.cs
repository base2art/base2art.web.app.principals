namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Collections.Specialized;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using App.Configuration;
    using ComponentModel.Composition;
    using Exceptions;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using Public.Controllers;

    public abstract class SingleAuthenticationModule : BaseAuthenticationModule, IAuthenticationModule, IAuthenticationScheme
    {
        protected abstract string AuthenticationScheme { get; }
        public sealed override string DefaultAuthenticationScheme => this.AuthenticationScheme;

        public virtual string Name => this.AuthenticationScheme;
        public virtual string DisplayName => this.GetType().Name.Replace("AuthenticationModule", "");

        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, services, config);

            services.Bind<IAuthenticationScheme>()
                    .As(ServiceLoaderBindingType.Singleton)
                    .To(this);

            services.Bind<IAuthenticationModule>()
                    .As(ServiceLoaderBindingType.Singleton)
                    .To(this);
        }

        protected async Task<ClaimsPrincipal> CreatePrincipal(string id, string email, string name)
        {
            // this.Stash();
            var claimsIdentity = new ClaimsIdentity(this.AuthenticationScheme, ClaimTypes.NameIdentifier, ClaimTypes.Role);
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, name));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Email, email));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, id));
            return new ClaimsPrincipal(claimsIdentity);
        }

        public override void RegisterUserServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterUserServices(builder, services, config);

            this.AuthorizationModule = services.Resolve<IAuthorizationModule>();
            
            var item = services.Resolve<IFakeService>(true);
            if (item != null)
            {
                return;
            }

            services.Bind<IFakeService>()
                    .To(new FakeService());
            services.Bind<PrimaryController>()
                    .To(x => new PrimaryController(x.ResolveAll<IAuthenticationScheme>(true), this.AuthorizationModule));

            var authBuilder = this.GetCookiesAuthBuilder(builder);

            var modules = services.ResolveAll<IAuthenticationModule>() ?? new IAuthenticationModule[0];

            foreach (var module in modules)
            {
                module.AddAuthentication(authBuilder);
            }
        }

        protected IAuthorizationModule AuthorizationModule { get; private set; }

        protected void ValidateDomain(string email, HttpContext context, string redirectUri)
        {
            var myRequiredDomain = this.AuthorizationModule?.RequiredDomain;

            if (string.IsNullOrWhiteSpace(myRequiredDomain))
            {
                return;
            }

            if (!email.ToLowerInvariant().EndsWith("@" + myRequiredDomain.ToLowerInvariant()))
            {
                this.HandleFail(context, redirectUri);
            }
        }

        protected void HandleFail(HttpContext context, string uri)
        {
            if (Uri.IsWellFormedUriString(uri, UriKind.RelativeOrAbsolute))
            {
                this.HandleFail(context, new Uri(uri));
            }
        }

        protected void HandleFail(HttpContext context, Uri uri)
        {
            NameValueCollection nvc = uri?.ParseQueryString();

            var returnUrl = nvc?["returnUrl"];

            var mess = Uri.EscapeUriString(FailureMessage.BadDomain.ToString("G"));
            var retUrl = Uri.EscapeUriString(returnUrl ?? "/");
            context.Response.Redirect($"/auth/sign-in?returnUrl={retUrl}&message={mess}", false);

            throw new RedirectException($"/auth/sign-in?returnUrl={retUrl}&message={mess}", false, false);
        }

        private class FakeService : IFakeService
        {
        }

        private interface IFakeService
        {
        }

        protected abstract void AddAuth(AuthenticationBuilder authBuilder);

        void IAuthenticationModule.AddAuthentication(AuthenticationBuilder authBuilder)
        {
            this.AddAuth(authBuilder);
        }
    }
}
//
// public interface IPlaceHolderService
// {
// }
/*
public class SmartAuthenticationHandler : AuthenticationHandler<SmartOptions>, IAuthenticationRequestHandler
{
    public SmartAuthenticationHandler(IOptionsMonitor<SmartOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
        : base(options, logger, encoder, clock)
    {
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        // this.Context.Response.Redirect("/auth/sign-in");
        // this.Context.Response.();
        // AuthenticateResult.Fail("", new AuthenticationProperties()).
        // Context.
        return AuthenticateResult.Fail("Failure");
    }

    public async Task<bool> HandleRequestAsync()
    {
        return false;
        // var cookie = this.Context.Request.Cookies["smart"];
        // return cookie == null;
    }
}

public class SmartOptions : AuthenticationSchemeOptions
{
}*/
// .AddScheme<>()
// .AddPolicyScheme("smart", "Authorization Bearer or OIDC", options =>
// {
//     options.ForwardDefaultSelector = context =>
//     {
//         return ""
//     };
//     // options.ForwardDefaultSelector = context =>
//     // {
//     //     var authHeader = context.Request.Headers["Authorization"].FirstOrDefault();
//     //     if (authHeader?.StartsWith("Bearer ") == true)
//     //     {
//     //         return JwtBearerDefaults.AuthenticationScheme;
//     //     }
//     //
//     //     return OpenIdConnectDefaults.AuthenticationScheme;
//     // };
// });
// .AddScheme<>();
// public override void RegisterSystemServicesPostUserService(
//     IMvcCoreBuilder builder,
//     IBindableServiceLoaderInjector services,
//     IServerConfiguration config)
// {
//     base.RegisterSystemServicesPostUserService(builder, services, config);
//
//     if (this.called)
//     {
//         return;
//     }
//
//     // https://stackoverflow.com/questions/45695382/how-do-i-setup-multiple-auth-schemes-in-asp-net-core-2-0
//     var authBuilder = this.GetAuthBuilder(builder);
//
//     this.AddAuth(authBuilder);
// }