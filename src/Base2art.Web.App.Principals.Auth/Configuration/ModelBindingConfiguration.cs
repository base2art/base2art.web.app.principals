namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Collections.Generic;
    using App.Configuration;

    public class ModelBindingConfiguration : IModelBindingConfiguration
    {
        public ModelBindingConfiguration(Type type, Type bindingType)
        {
            this.BoundType = type;
            this.BinderType = bindingType;
        }

        public Type BoundType { get; }
        public Type BinderType { get; }
        public IReadOnlyDictionary<string, object> BinderParameters { get; } = new Dictionary<string, object>();
        public IReadOnlyDictionary<string, object> BinderProperties { get; } = new Dictionary<string, object>();
        public BindingType BindingType { get; } = BindingType.Hidden;
    }
}