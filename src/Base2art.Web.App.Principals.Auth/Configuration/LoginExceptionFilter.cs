namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Exceptions;
    using Filters;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Http.Extensions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    internal class LoginExceptionFilter : IAsyncExceptionFilter //, IExceptionFilter
    {
        private readonly string defaultAuthenticationScheme;

        public string CookieName { get; }
        public LoginExceptionFilter(string defaultAuthenticationScheme, string cookieName)
        {
            this.defaultAuthenticationScheme = defaultAuthenticationScheme;
            this.CookieName = cookieName;
        }

        /// <inheritdoc />
        public virtual Task OnExceptionAsync(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var contextException = context.Exception;
            
            switch (contextException)
            {
                // if (string.Equals(contextException?.GetType()?.Name, "SignOutException", StringComparison.Ordinal))
                case SignOutException so:
                    return this.HandleSignOut(context, so);
                case NotAuthenticatedException b:
                    return HandleSignIn(context, b);
                default:
                    return Task.CompletedTask;
            }
        }

        private static Task HandleSignIn(ExceptionContext context, NotAuthenticatedException b)
        {
            var authenticationType = ""; //this.defaultAuthenticationScheme;
            if (b.Data.Contains("AuthenticationType"))
            {
                authenticationType = b.Data["AuthenticationType"] as string ?? string.Empty;
            }

            if (string.IsNullOrWhiteSpace(authenticationType))
            {
                b.SuppressLogging();
                context.Result = new RedirectResult("/auth/sign-in?returnUrl=" + context.HttpContext.Request.GetEncodedPathAndQuery());
                context.ExceptionHandled = true;
                return Task.CompletedTask;
            }

            b.SuppressLogging();

            context.Result = new ChallengeResult(authenticationType,
                                                 new AuthenticationProperties
                                                 {
                                                     RedirectUri = context.HttpContext.Request.GetDisplayUrl()
                                                 });
            context.ExceptionHandled = true;
            return Task.CompletedTask;
        }

        private Task HandleSignOut(ExceptionContext context, Exception contextException)
        {
            contextException.SuppressLogging();
            var returnUrl = "/";
            if (contextException.Data.Contains("returnUrl"))
            {
                returnUrl = contextException.Data["returnUrl"] as string;
                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    returnUrl = "/";
                }
            }
            
            context.HttpContext.Response.Cookies.Delete(this.CookieName);
            context.Result = new RedirectResult(returnUrl);
            context.ExceptionHandled = true;
            return Task.CompletedTask;
        }

    }
}