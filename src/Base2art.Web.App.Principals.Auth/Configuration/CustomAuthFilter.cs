namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System;
    using System.Threading.Tasks;
    using Exceptions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http.Extensions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Authorization;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class CustomAuthFilter : AuthorizeFilter
    {
        public CustomAuthFilter(AuthorizationPolicy policy)
            : base(policy)
        {
        }

        public override async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var request = context.HttpContext.Request;
            if (string.IsNullOrWhiteSpace(request.Protocol) &&
                string.IsNullOrWhiteSpace(request.Scheme) &&
                string.IsNullOrWhiteSpace(request.Path) &&
                string.IsNullOrWhiteSpace(request.Host.Host))
            {
                return; //Task.CompletedTask;
            }

            if (request.Path.HasValue)
            {
                var parts = request.Path.Value.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 1)
                {
                    if (string.Equals(parts[0], "auth", StringComparison.OrdinalIgnoreCase))
                    {
                        return; // Task.CompletedTask;
                    }
                }
            }

            // throw new NotAuthenticatedException();
            await base.OnAuthorizationAsync(context);
            if (context.Result?.GetType() == typeof(ForbidResult) || context.Result?.GetType() == typeof(ChallengeResult))
            {
                // throw new  NotAuthenticatedException("Not Authetnicated");
                context.Result = new RedirectResult("/auth/sign-in?returnUrl=" + context.HttpContext.Request.GetEncodedPathAndQuery());
            }
        }

        /*
        private MvcOptions _mvcOptions;
        private AuthorizationPolicy _effectivePolicy;

        private async Task<AuthorizationPolicy> GetEffectivePolicyAsync(AuthorizationFilterContext context)
        {
            if (_effectivePolicy != null)
            {
                return _effectivePolicy;
            }

            var effectivePolicy = await ComputePolicyAsync();
            var canCache = PolicyProvider == null;

            if (_mvcOptions == null)
            {
                _mvcOptions = context.HttpContext.RequestServices.GetRequiredService<IOptions<MvcOptions>>().Value;
            }

            if (_mvcOptions.AllowCombiningAuthorizeFilters)
            {
                if (!context.IsEffectivePolicy(this))
                {
                    return null;
                }

                // Combine all authorize filters into single effective policy that's only run on the closest filter
                var builder = new AuthorizationPolicyBuilder(effectivePolicy);
                for (var i = 0; i < context.Filters.Count; i++)
                {
                    if (ReferenceEquals(this, context.Filters[i]))
                    {
                        continue;
                    }

                    if (context.Filters[i] is CustomAuthFilter authorizeFilter)
                    {
                        // Combine using the explicit policy, or the dynamic policy provider
                        builder.Combine(await authorizeFilter.ComputePolicyAsync());
                        canCache = canCache && authorizeFilter.PolicyProvider == null;
                    }
                }

                effectivePolicy = builder?.Build() ?? effectivePolicy;
            }

            // We can cache the effective policy when there is no custom policy provider
            if (canCache)
            {
                _effectivePolicy = effectivePolicy;
            }

            return effectivePolicy;
        }

        public virtual async Task OnAuthorizationAsync1(AuthorizationFilterContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var effectivePolicy = await GetEffectivePolicyAsync(context);
            if (effectivePolicy == null)
            {
                return;
            }

            var policyEvaluator = context.HttpContext.RequestServices.GetRequiredService<IPolicyEvaluator>();

            var authenticateResult = await policyEvaluator.AuthenticateAsync(effectivePolicy, context.HttpContext);

            // Allow Anonymous skips all authorization
            if (HasAllowAnonymous(context.Filters))
            {
                return;
            }

            var authorizeResult = await policyEvaluator.AuthorizeAsync(effectivePolicy, authenticateResult, context.HttpContext, context);

            if (authorizeResult.Challenged)
            {
                context.Result = new ChallengeResult(effectivePolicy.AuthenticationSchemes.ToArray());
            }
            else if (authorizeResult.Forbidden)
            {
                context.Result = new ForbidResult(effectivePolicy.AuthenticationSchemes.ToArray());
            }
        }

        // Computes the actual policy for this filter using either Policy or PolicyProvider + AuthorizeData
        private Task<AuthorizationPolicy> ComputePolicyAsync()
        {
            if (Policy != null)
            {
                return Task.FromResult(Policy);
            }

            if (PolicyProvider == null)
            {
                throw new InvalidOperationException($"Cannot create Policy {nameof(AuthorizationPolicy)} {nameof(IAuthorizationPolicyProvider)}");
            }

            return AuthorizationPolicy.CombineAsync(PolicyProvider, AuthorizeData);
        }

        private static bool HasAllowAnonymous(IList<IFilterMetadata> filters)
        {
            for (var i = 0; i < filters.Count; i++)
            {
                if (filters[i] is IAllowAnonymousFilter)
                {
                    return true;
                }
            }

            return false;
        }*/
    }
}