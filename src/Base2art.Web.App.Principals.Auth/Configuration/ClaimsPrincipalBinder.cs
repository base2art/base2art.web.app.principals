namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public class ClaimsPrincipalBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var item = bindingContext.HttpContext?.User ?? this.NullAuth();
            bindingContext.Result = ModelBindingResult.Success(item);
            return Task.CompletedTask;
        }

        private ClaimsPrincipal NullAuth() => new ClaimsPrincipal(new ClaimsIdentity());
    }
}