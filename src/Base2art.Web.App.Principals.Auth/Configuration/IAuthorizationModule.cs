namespace Base2art.Web.App.Principals.Auth.Configuration
{
    public interface IAuthorizationModule
    {
        string RequiredDomain { get; }
    }
}