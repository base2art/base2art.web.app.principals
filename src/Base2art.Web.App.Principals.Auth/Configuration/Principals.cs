namespace Base2art.Web.App.Principals.Auth.Configuration
{
    using System.Security.Claims;

    public static class Principals
    {
        public static string GetValue(this ClaimsPrincipal principal, string value)
            => principal?.FindFirst(value)?.Value ?? string.Empty;
    }
}