namespace Base2art.Web.App.Principals.AzureAdAuth
{
    using System;
    using System.IO;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Auth.Configuration;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Authentication.OAuth;
    using Microsoft.AspNetCore.Authentication.OpenIdConnect;
    using Microsoft.Extensions.DependencyInjection;

    public class AzureAdAuthenticationModule : SingleAuthenticationModule
    {
        private readonly string clientId;
        private readonly string tenantId;
        private readonly string clientSecret;

        public AzureAdAuthenticationModule(
            string clientId,
            string tenantId,
            string clientSecret)
        {
            if (clientId == "#{AzureAdAuth_ClientId}" && tenantId == "#{AzureAdAuth_TenantId}" && clientSecret == "#{AzureAdAuth_ClientSecret}")
            {
                var googleAuthClientId = Path.Combine(
                                                      Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                      ".config",
                                                      "base2art",
                                                      "deployment",
                                                      "coordinator",
                                                      "AzureAdAuth_ClientId");
                var googleAuthClientSecret = Path.Combine(
                                                          Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                          ".config",
                                                          "base2art",
                                                          "deployment",
                                                          "coordinator",
                                                          "AzureAdAuth_ClientSecret");
                var googleAuthTenantId = Path.Combine(
                                                      Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                                                      ".config",
                                                      "base2art",
                                                      "deployment",
                                                      "coordinator",
                                                      "AzureAdAuth_TenantId");
                this.clientId = File.ReadAllText(googleAuthClientId).Trim();
                this.clientSecret = File.ReadAllText(googleAuthClientSecret).Trim();
                this.tenantId = File.ReadAllText(googleAuthTenantId).Trim();
            }
            else
            {
                this.clientId = clientId;
                this.tenantId = tenantId;
                this.clientSecret = clientSecret;
            }
        }

        protected override string AuthenticationScheme => "azure-ad";

        protected override void AddAuth(AuthenticationBuilder authBuilder)
        {
            authBuilder.AddOpenIdConnect(this.AuthenticationScheme, this.DisplayName, options =>
            {
                options.ClientId = this.clientId;
                options.ClientSecret = this.clientSecret;
                options.Authority = $"https://login.microsoftonline.com/{this.tenantId:D}";
                // options.SignedOutRedirectUri = "http://localhost:5000/";

                options.SignInScheme = "Cookies";
                options.ResponseType = "code id_token";
                options.CallbackPath = "/auth/azure-ad/callback";
                options.Scope.Add("email");
                options.Scope.Add("profile");
                // options.CallbackPath = "/signin-oidc";
                options.GetClaimsFromUserInfoEndpoint = false;

#if NETCOREAPP3_1_OR_GREATER
                options.CorrelationCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
                options.NonceCookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Unspecified;
#endif

                options.UseTokenLifetime = true;

                options.Events.OnTokenValidated += async arg =>
                {
                    var given = arg.Principal.GetValue(ClaimTypes.GivenName);
                    var surname = arg.Principal.GetValue(ClaimTypes.Surname);
                    var name = arg.Principal.GetValue("name");

                    if (!string.IsNullOrWhiteSpace(given) && !string.IsNullOrWhiteSpace(surname))
                    {
                        name = string.Concat(given, " ", surname);
                    }

                    var email = arg.Principal.GetValue(ClaimTypes.Email);
                    this.ValidateDomain(email, arg.HttpContext, arg.Properties.RedirectUri);

                    arg.Principal = await this.CreatePrincipal(
                                                               arg.Principal.GetValue(ClaimTypes.NameIdentifier),
                                                               email,
                                                               name);
                };
            });
        }
    }
}